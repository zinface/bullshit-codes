import org.apache.commons.lang3.StringUtils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class TimeConversion  {

    private void convertTypeToTime(UserCourseUsageDetailParamDto userCourseUsageDetailParamDto, int type, String startTime, String endTime) {
        try {
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            if(type ==6){
                DateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd");
                Calendar cal = Calendar.getInstance();
                if(!StringUtils.isEmpty(startTime)){
                    cal.setTime(dateFormat1.parse(startTime));
                    userCourseUsageDetailParamDto.setStartDate(dateFormat.format(cal.getTime()));
                }
                if(!StringUtils.isEmpty(endTime)){
                    userCourseUsageDetailParamDto.setEndDate(dateFormat.format(dateFormat1.parse(endTime)));
                }
            }else {
                Map<String,String> dateMap = getDateByType(type);
                userCourseUsageDetailParamDto.setStartDate(dateMap.get("startDate"));
                userCourseUsageDetailParamDto.setEndDate(dateMap.get("endDate"));
            }
        } catch (ParseException e) {
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date startDate = new Date();
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(startDate);
            String startStr = null , endStr = null;
            endStr=dateFormat.format(startDate);
            calendar.add(Calendar.YEAR, 0);
            calendar.set(Calendar.DAY_OF_YEAR, 1);
            startStr = dateFormat.format(calendar.getTime());
            userCourseUsageDetailParamDto.setStartDate(startStr);
            userCourseUsageDetailParamDto.setEndDate(endStr);
        }
    }

    private Map<String, String> getDateByType(int type){
        Map<String,String> seDate = new HashMap<>();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date startDate = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(startDate);
        String startStr = null , endStr = null;
        if(type==0){
            endStr=dateFormat.format(startDate);
            calendar.add(Calendar.DATE,-1);
            startStr = dateFormat.format(calendar.getTime());
        }else if(type==1){
            endStr=dateFormat.format(startDate);
            calendar.add(Calendar.DATE,-7);
            startStr = dateFormat.format(calendar.getTime());
        }else if(type==2){
            endStr=dateFormat.format(startDate);
            calendar.add(Calendar.DATE,-30);
            startStr = dateFormat.format(calendar.getTime());
        }else if(type==3){
            endStr=dateFormat.format(startDate);
            calendar.add(Calendar.MONTH, 0);
            calendar.set(Calendar.DAY_OF_MONTH, 1);
            startStr = dateFormat.format(calendar.getTime());
        }else if(type==4){
            DateFormat dateFormat1 = new SimpleDateFormat("MM-dd");
            DateFormat dateFormat2 = new SimpleDateFormat("yyyy");
            endStr=dateFormat.format(startDate);
            String newStr =  dateFormat1.format(startDate);
            if(Integer.valueOf(newStr)>=201 && Integer.valueOf(newStr) <= 813){
                startStr = dateFormat2.format(startDate)+"0201";
            }else {
                if(Integer.valueOf(newStr)>=101){
                    startStr = (Integer.valueOf(dateFormat2.format(startDate))-1)+"0814";
                }else {
                    startStr = dateFormat2.format(startDate)+"0814";
                }
            }
        }else if(type==5){
            endStr=dateFormat.format(startDate);
            calendar.add(Calendar.YEAR, 0);
            calendar.set(Calendar.DAY_OF_YEAR, 1);
            startStr = dateFormat.format(calendar.getTime());
        }
        seDate.put("startDate",startStr);
        seDate.put("endDate",endStr);
        return seDate;
    }
}
