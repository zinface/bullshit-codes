package com.li.common.valid;

/**
 * 判断字符串是否等于某个固定值
 *  PS:(某位不愿透露姓名的三年JAVA开发工程师编写的业务代码)
 **/
public class StringEquals {
    public static void main(String[] args) {
        String code="TestC";
        ifEqual(code);
    }

    public static String ifEqual(String code){
        /**
         * String类已经重写了equals()方法,会判断值和内存地址
         * 使用 | 判断
         * |做运算为或运算  相同为0,不同为1
         * | 做判断无短路操作
         */

        if(code=="TestA" | code.equals("TestA")){
            //做某项业务
           return "TestA";

        }else if(code=="TestB" | code.equals("TestB")){
            //做某项业务
            return "TestB";

        }
        return null;
    }
}
